const fs = require('fs');
const http = require('http');

const port = process.env.PORT || 8000;

const server = http.createServer((req,res)=>{

    res.statusCode=200;
    res.setHeader('Content-Type','text/html')
    console.log(req)
    if(req.url=="/"){
        res.statusCode=200;
        const data = fs.readFileSync('first.html');
        // res.end('<h1>i am harsh saini!btech</h1> <p>this my node js server!http</p>');
        res.end(data.toString());
    }
    else if(req.url=="/about"){
        res.statusCode=200;
        res.end('<h1>about harsh saini!btech</h1> <p>this is about node js server!http</p>');
    }
    else if(req.url=="/contact"){
        res.statusCode=200;
        res.end('<h1>contact to harsh saini!btech</h1> <p>this is a contact of node js server!http</p>');
    }
    else if(req.url=="/action"){
        res.statusCode=200;
        res.end('<h1>Action Page!</h1> <p>this is an action page..</p>');
    }
    else if(req.url=="/action2"){
        res.statusCode=200;
        res.end('<h1>Another Action Page!</h1> <p>this is an another action page.....</p>');
    }
    else if(req.url=="/action3"){
        res.statusCode=200;
        res.end('<h1>Something Else Here!</h1> <p>this is an another page</p>');
    }
    else{
        res.statusCode=404;
        res.end('<h1>error 404 page not found</h1> <p>this is page is not found on this server</p>');
    }
    

})
server.listen(port,()=>{
    console.log(`server is listening on port http://localhost:${port}`);
});